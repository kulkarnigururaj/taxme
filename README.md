# README #

This web application was developed using MVC 5 and EF6 on VS 2015. And by default it connects to SQL Server and performs database operations on Master

### Why? ###

* This web application processes your account transactions data to further calculate tax figures.
* You can upload your account transactions in a CSV file format using the ***Upload Transactions*** menu option.
* ***View Transactions*** menu option will let you view all your transactions in a paged view.
* I haven't used AJAX so when you perform some operation please wait for it to complete. I know this is not the best experience.

### Set up ###

* By default I am using SQL server and working on "Master" database.
* The connection string can be found in web.config and feel free to change the database name if you prefer.
* The SQL scripts are provided. 
* Unity is used for IoC
* Bootstrap CSS 
* For this assignment I have just added all the files (abstraction and implementation) to the same project.