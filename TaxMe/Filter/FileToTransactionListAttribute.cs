﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TaxMe.Extensions;
using TaxMe.Models;

namespace TaxMe.Filter
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FileToTransactionListAttribute : ActionFilterAttribute
    {
        private readonly string parameterName;

        public FileToTransactionListAttribute(string parameterName)
        {
            this.parameterName = parameterName;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var valueProviderResult = filterContext.Controller.ValueProvider.GetValue(this.parameterName);
            var transactions = new List<ITransaction>();

            if (valueProviderResult != null)
            {
                HttpPostedFileBase uploadFile = ((HttpPostedFileBase[])valueProviderResult.RawValue)?.SingleOrDefault();

                if (uploadFile != null)
                {
                    var allLines = ReadLines(uploadFile.InputStream, Encoding.UTF8);

                    transactions = allLines.AsParallel()
                                           .Select(x => CastRowToTransaction(x, filterContext))
                                           .Where (x => x != null)
                                           .ToList();
                }
            }

            if (!transactions.Any())
            {
                filterContext.Controller.ViewData.ModelState.AddModelError(this.parameterName, "Please upload a valid csv file.");
                filterContext.Result = new ViewResult { ViewData = filterContext.Controller.ViewData };
            }
            else
            {
                var firstParameter = filterContext.ActionParameters.Select(x => x.Key).First();
                filterContext.ActionParameters[firstParameter] = transactions;
            }
        }

        private IEnumerable<string> ReadLines(Stream inputStream, Encoding encoding)
        {
            using (StreamReader sr = new StreamReader(inputStream, encoding))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }

        private static Transaction CastRowToTransaction(string line)
        {
            var itemArray = line.Split(",".ToCharArray());

            if (itemArray.Count() != 4)
            {
                return null;
            }

            return new Transaction
            {
                Account = itemArray[0],
                Description = itemArray[1],
                CurrencyCode = itemArray[2],
                Amount = itemArray[3].ToNullableDecimal()
            };
        }

        private ITransaction CastRowToTransaction(string line, ActionExecutingContext filterContext)
        {
            var itemArray = line.Split(",".ToCharArray());

            if (itemArray.Count() != 4)
            {
                return null;
            }

            var transaction = new Transaction
            {
                Account = itemArray[0],
                Description = itemArray[1],
                CurrencyCode = itemArray[2],
                Amount = itemArray[3].ToNullableDecimal()
            };

            var context = new ValidationContext(transaction);
            var results = new List<ValidationResult>();

            if (Validator.TryValidateObject(transaction, context, results, true))
            {
                return transaction;
            }

            return new TransactionError
            {
                Account = transaction.Account,
                Description = transaction.Description,
                CurrencyCode = transaction.CurrencyCode,
                Amount = transaction.Amount,
                ErrorMessage = string.Join("|", results.Select(x => x.ErrorMessage))
            };
        }
    }
}