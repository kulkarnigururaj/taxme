namespace TaxMe.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostConfiguration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "Account", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Transactions", "CurrencyCode", c => c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false));
            AlterColumn("dbo.Transactions", "Amount", c => c.Decimal(nullable: false, precision: 7, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Transactions", "CurrencyCode", c => c.String());
            AlterColumn("dbo.Transactions", "Account", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
