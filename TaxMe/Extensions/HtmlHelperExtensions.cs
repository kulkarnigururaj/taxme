﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TaxMe
{
    public static class HtmlHelperExtensions
    {
        public static string IsActive(this HtmlHelper html, string controller = "Home", string activeClass = "active")
        {
            ViewContext viewContext = html.ViewContext;
            RouteValueDictionary routeValues = viewContext.RouteData.Values;

            string currentController = routeValues["controller"].ToString();

            return string.Compare(currentController, controller, true) == 0 ? activeClass : string.Empty;
        }
    }
}