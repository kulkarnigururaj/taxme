﻿namespace TaxMe.Extensions
{
    public static class DecimalExtensions
    {
        public static decimal? ToNullableDecimal(this string value)
        {
            decimal amountVal;
            return decimal.TryParse(value, out amountVal) ? amountVal : default(decimal?);
        }
    }
}