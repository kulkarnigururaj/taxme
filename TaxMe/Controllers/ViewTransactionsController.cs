﻿using System.Web.Mvc;
using TaxMe.Models;
using TaxMe.Services;

namespace TaxMe.Controllers
{
    public class ViewTransactionsController : Controller
    { 
        private readonly ITransactionsService transactionsService;

        public ViewTransactionsController(ITransactionsService transactionsService)
        {
            this.transactionsService = transactionsService;
        }

        // GET: ViewTransactions
        public ActionResult Index(int? page)
        {
            var dbContext = new TaxMeDb();

            dbContext.Database.Log = message => System.IO.File.AppendAllLines("C:\\temp\\mylog.txt", new[] { message }); 
            return View(this.transactionsService.GetPagedTransactionList<Transaction>(page ?? 1));
        }
    }
}