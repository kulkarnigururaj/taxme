﻿using System.Web.Mvc;

namespace TaxMe.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}