﻿using System.Collections.Generic;
using System.Web.Mvc;
using TaxMe.Filter;
using TaxMe.Models;
using TaxMe.Services;

namespace TaxMe.Controllers
{
    public class UploadTransactionsController : Controller
    {
        private readonly ITransactionsService transactionsService;

        public UploadTransactionsController(ITransactionsService transactionsService)
        {
            this.transactionsService = transactionsService;
        }

        // GET: UploadTransactions
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FileToTransactionList("fileUpload")]
        public ActionResult Index(List<ITransaction> transactions)
        {
            var transactionsUploaded = this.transactionsService.ProcessTransactions(transactions);
            return RedirectToAction("UploadResults", new { noOfTransactionsUploaded = transactionsUploaded });
        }

        public ActionResult UploadResults(int noOfTransactionsUploaded, int? page)
        {
            ViewBag.NoOfTransactionUploaded = noOfTransactionsUploaded;
            var model = this.transactionsService.GetPagedTransactionList<TransactionError>(page ?? 1);

            return View(model);
        }
    }
}