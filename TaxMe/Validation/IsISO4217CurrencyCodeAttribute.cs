﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.Practices.Unity;
using TaxMe.Services;

namespace TaxMe.Validation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class IsISO4217CurrencyCodeAttribute : ValidationAttribute
    {
        private readonly ICurrencyCodeService currencyCodeService;

        public IsISO4217CurrencyCodeAttribute()
        {
            this.currencyCodeService = UnityConfig.GetConfiguredContainer().Resolve<ICurrencyCodeService>();
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var currencyCode = value as string;

            if (string.IsNullOrEmpty(currencyCode))
            {
                return new ValidationResult($"{validationContext.DisplayName} is required field.");
            }

            currencyCode = currencyCode.ToUpperInvariant();

            if (!this.currencyCodeService.GetAll().Contains(currencyCode))
            {
                return new ValidationResult($"{value} is an invalid ISO 4217 currency code.");
            }

            return null;
        }
    }
}