﻿(function () {

    var bar = $('.progress');
    var percent = $('.progress-bar');
    var status = $('#status');

    $('form').ajaxForm({
        beforeSend: function () {
            status.empty();
            status.hide();
            bar.show();
            var percentVal = '0%';
            percent.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            percent.width(percentVal)
            percent.html(percentVal);
        },
        success: function () {
        },
        complete: function (xhr) {
            status.html(xhr.responseText);
            var percentVal = '100%';
            percent.width(percentVal)
            percent.html(percentVal);
            status.show();
            bar.hide();
        }
    });

})();