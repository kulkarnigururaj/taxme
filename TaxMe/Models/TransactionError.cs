﻿using System.ComponentModel.DataAnnotations;

namespace TaxMe.Models
{
    public class TransactionError : ITransaction
    {
        public int Id { get; set; }

        public string Account { get; set; }

        public string Description { get; set; }

        public string CurrencyCode { get; set; }

        public decimal? Amount { get; set; }

        [Required]
        public string ErrorMessage { get; set; }
    }
}