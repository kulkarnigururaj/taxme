﻿namespace TaxMe.Models
{
    public interface ITransaction
    {
        int Id { get; set; }

        string Account { get; set; }

        string Description { get; set; }

        string CurrencyCode { get; set; }

        decimal? Amount { get; set; }
    }
}
