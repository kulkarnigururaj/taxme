﻿using System.ComponentModel.DataAnnotations;
using TaxMe.Validation;

namespace TaxMe.Models
{
    public class Transaction : ITransaction
    {
        public int Id { get; set; }

        [StringLength(20, ErrorMessage = "Account should not exceed 10 characters.")]
        [Required]
        public string Account { get; set; }

        [StringLength(20, ErrorMessage = "Description should not exceed 20 characters.")]
        [Required]
        public string Description { get; set; }

        [IsISO4217CurrencyCode]
        [Required]
        public string CurrencyCode { get; set; }

        [Required]
        [RegularExpression(@"^\d{1,5}(\.\d{1,2})?$", ErrorMessage = "Amount must not of the format xxxxx.xx")]
        public decimal? Amount { get; set; }
    }
}