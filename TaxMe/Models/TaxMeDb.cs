﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace TaxMe.Models
{
    public class TaxMeDb : DbContext
    {
        public TaxMeDb()
            : base("name=TaxMeContext")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
        }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionError> TransactionErrors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<Transaction>()
                .Property(p => p.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Transaction>()
                .Property(p => p.Account)
                .HasMaxLength(10)
                .IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(p => p.Description)
                .HasMaxLength(20)
                .IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(p => p.CurrencyCode)
                .HasMaxLength(3)
                .HasColumnType("char")
                .IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(p => p.Amount)
                .HasPrecision(7,2)
                .IsRequired();

            modelBuilder.Entity<TransactionError>()
                        .HasKey(p => p.Id);

            modelBuilder.Entity<TransactionError>()
                        .Property(p => p.Id)
                        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            base.OnModelCreating(modelBuilder);
        }
    }
}