﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace TaxMe.Services
{
    public class ISO4217CurrencyCodeService : ICurrencyCodeService
    {
        private List<string> validCurrencyCodes = new List<string>();

        public IEnumerable<string> GetAll()
        {
            if (!this.validCurrencyCodes.Any())
            {
                // Of course this could have been fetched from a database table.
                this.validCurrencyCodes = CultureInfo.GetCultures(CultureTypes.InstalledWin32Cultures)
                                                     .Where(x => !string.IsNullOrEmpty(x.Name) && !x.IsNeutralCulture)
                                                     .Select(x => new RegionInfo(x.LCID).ISOCurrencySymbol.ToUpperInvariant())
                                                     .Distinct()
                                                     .ToList();
            }

            return this.validCurrencyCodes;
        }
    }
}