﻿using System;
using System.Collections.Generic;
using System.Linq;
using PagedList;
using TaxMe.Models;

namespace TaxMe.Services
{
    public class TransactionsService : ITransactionsService
    {
        public int ProcessTransactions(IEnumerable<ITransaction> transactions)
        {
            var noOfTransactionsUploaded = 0;

            using (var dbContext = new TaxMeDb())
            {
                // First bulk insert all the valid transactions
                var validTransactions = transactions.OfType<Transaction>();
                noOfTransactionsUploaded = validTransactions.Count();

                if (validTransactions.Any())
                {
                    dbContext.BulkInsert(validTransactions);
                }

                // Clear the old error results
                dbContext.Database.ExecuteSqlCommand("TRUNCATE TABLE TransactionErrors");

                // Bulk upload the new errors
                var transactionInErrors = transactions.OfType<TransactionError>();

                if (transactionInErrors.Any())
                {
                    dbContext.BulkInsert(transactionInErrors);
                }
            }

            return noOfTransactionsUploaded;
        }

        public IPagedList<TEntity> GetPagedTransactionList<TEntity>(int page, int pageSize = 20) where TEntity : class, ITransaction
        {
            IPagedList<TEntity> pagedTransactions = null;
            using (var dbContext = new TaxMeDb())
            {
                //dbContext.Database.Log = message => System.IO.File.AppendAllLines("C:\\temp\\mylog.txt", new[] { message });
                pagedTransactions =  dbContext.Set<TEntity>().AsNoTracking().OrderBy(x => x.Id).ToPagedList(page, pageSize);
            }

            return pagedTransactions;
        }
    }
}