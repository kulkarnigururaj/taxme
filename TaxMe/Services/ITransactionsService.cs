﻿using System.Collections.Generic;
using PagedList;
using TaxMe.Models;

namespace TaxMe.Services
{
    public interface ITransactionsService
    {
        int ProcessTransactions(IEnumerable<ITransaction> transactions);

        IPagedList<TEntity> GetPagedTransactionList<TEntity>(int page, int pageSize = 20) where TEntity : class, ITransaction;
    }
}
