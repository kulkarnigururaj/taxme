﻿using System.Collections.Generic;

namespace TaxMe.Services
{
    public interface ICurrencyCodeService
    {
        IEnumerable<string> GetAll();
    }
}
